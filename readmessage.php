<!--
* Created by Abdullah_Kabir on 18/05/2017.
-->
<?php 

require_once "function.php";
require_once "dbconnect.php";
session_start();
confirmLoggedIn();
?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animated.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/imagehover.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/animated.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>




    <!-- Custom Font -->
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font";
            src: url('fonts/BalooThambi-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font1";
            src: url('fonts/Lobster-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        .card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color: #ffffff;
    display: inline-block;

}
    </style>



    <!--Javascript-->
    <script type = "text/javascript" src= "js/dropdown.js" >
    </script>

    <script type = "text/javascript" src= "js/sticky.js" >
    </script>

    <script src="js/jquery-3.1.1.slim.min.js"></script>
    <script src="js/bootstrap.js"></script>

    <script type = "text/javascript" src= "js/npm.js" >
    </script>

    <script type = "text/javascript" src= "js/tooltip.js" >
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!--js Finishes-->

    <!--Browser Topbar-->
    <link rel="shortcut icon" href="images/img-01.png">
    <title>Insight[News Feed]</title>

    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>

</head>

<!-----------------------------------------Nav Bar------------------------->
<?php include 'navbar.php'; ?>




<!--Test-->
<div class="project-display" id="projects">

    <h2 id="project">My Message</h2>
    <h2 id="project" style="border-top-width: 3px;border-top-style: solid;" ></h2><br><br><br>


<form action="replymessage.php">
<?php 
      
     

        $StoringUsername1 = getUserName();

       $query1 = "select  UID from user where USER_NAME = '$StoringUsername1'";
       $result1 = mysqli_query($connection, $query1);
        while($row = mysqli_fetch_assoc($result1)){
     
     $FromIDforMSG = $row['UID']; //Storing user id into UDI variable
     
    // echo $UID;
 }   

    

      /* $query = mysqli_query($connection, "SELECT MSG FROM message where From_UID = $FromIDforMSG or TO_UID = $FromIDforMSG " );*/

      $query = mysqli_query($connection, "SELECT * FROM message where FROM_UID=$FromIDforMSG or TO_UID = $FromIDforMSG ");

        while($row = mysqli_fetch_assoc($query))

            { 
                
                $messageId= $row['MID']; 
                $profilePic = '<div class="media" style="margin-top: 50px;">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-top: 38px;">
            <div class="hovereffect4">';

                $profilePic.= "<img class=";
                $profilePic .="'img-responsive'" ;
                $profilePic .=" style='margin-left:50px; height:200px ;width:200px; border-radius: 200px;' src='data:image/jpeg;base64,{$row["image"]}'>";
                $profilePic.= '<div class="overlay4">
                    <h2>'.$row['USER_NAME'].'</h2>
                    <p>';
                  
                    $profilePic .="<button id=";
                    $profilePic .='"PostId"';
                    $profilePic .=" name=";
                    $profilePic .= "textno";
                    $profilePic .=" value=".'"'.$messageId.'"' ."'>Massage</button>
                </p>

                </div>
            </div>
        </div>";
        echo $profilePic;
                 
                 $post = "<div class=";
                 $post .="media-body";
                 $post .="> <h4 id=";
                 $post .="android_index_heading";
                 $post .=" class=";
                 $post .="media-heading";
                 $post .=">".$row['USER_NAME']."<div style='float: right; margin-top: 10px;' id= 'jsfnt1'> <br>at[".$row['DATE']."]"."</div></h4><h5 id=";
                 $post .='"jsfnt1"';
                 $post .=" style=";
                 $post .='"border-top-width: 1px;border-top-style: solid;"';
                 $post .=">".$row['MSG']."</h5>";
                 $post .= ' </div>
    </div>';
                // $post =   "<div class ="."card"." data-animate-effect="."fadeIn".">"."Posted By: "."<td>".$row['USER_NAME']."</td>";
                // $post .= $row['CONTENT']."</div>";
                echo $post;

            
  

        }

        

        
 ?>
   
</form>

<div class="gototop js-top" style="float: right;">
        <a href="#" class="js-gotop"><img src="img/top.png"></a>
    </div>

<!--Footer-->
<div id="Footer">

    <img  id="footer_img" class="flip infinite animated" src="logo.png">
    <?php include 'footer.php'; ?> 
</div>
</body>
</html>