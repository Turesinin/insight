<?php
require_once "function.php";
require_once "dbconnect.php";
session_start();
confirmLoggedIn();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>


    
    
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animated.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/imagehover.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/animated.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>




    <!-- Custom Font -->
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font";
            src: url('fonts/BalooThambi-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font1";
            src: url('fonts/Lobster-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        .card2 {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color: #ffffff;
    display: inline-block;
    float: right;
    margin-right: 250px; 
}
.card {
  
  max-width: 30%;
  float: left;
  margin: auto;
  margin-left: 50px;
  text-align: center;
  font-family: arial;
  background-color: rgba(0, 0, 0, 0.0);
}

.card:hover {
    box-shadow: 0 12px 16px 0 rgba(0,0,0,0.2);
}


.title {
  color: wheat;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}



button:hover, a:hover {
  opacity: 0.7;
}
    </style>



    <!--Javascript-->
    <script type = "text/javascript" src= "js/dropdown.js" >
    </script>

    <script type = "text/javascript" src= "js/sticky.js" >
    </script>

    <script src="js/jquery-3.1.1.slim.min.js"></script>
    <script src="js/bootstrap.js"></script>

    <script type = "text/javascript" src= "js/npm.js" >
    </script>

    <script type = "text/javascript" src= "js/tooltip.js" >
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!--js Finishes-->

    <!--Browser Topbar-->
    <link rel="shortcut icon" href="images/img-01.png">

    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>

</head>

<script src = "ckeditor/ckeditor.js"></script>

<?php include 'navbar.php'; ?>
<div >
<img src="img/load.gif" style="margin-left: auto;
	margin-right: auto;
	display: block;
	height: 250px;
	width: 250px;"
	>
	<h1 style="text-align: center; color: wheat;"> Done! </h1>
</div>

<div id="Footer">

    <img  id="footer_img" class="flip infinite animated" src="logo.png">
    <?php include 'footer.php'; ?> 
</div>
</html>
</body>
