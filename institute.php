<!DOCTYPE html>
<html>
<head>
<style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial;
    padding: 10px;
    background: #f1f1f1;
}

/* Header/Blog Title */
.header {
    padding: 30px;
    text-align: center;
    background: white;
    background-image: url(img/autheader.jpg);
  

}

.header h1 {
    font-size: 50px;
}

/* Style the top navigation bar */
.topnav {
    overflow: hidden;
    background-color: #333;
}

/* Style the topnav links */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
    float: left;
    width: 75%;
}

/* Right column */
.rightcolumn {
    float: left;
    width: 25%;
    background-color: #f1f1f1;
    padding-left: 20px;
}

/* Fake image */
.fakeimg {
    background-color: #FFF;/* This is the background ogf image color..Before it was ash or #aaa.*/
    width: 100%;
    padding: 20px;
}

/* Add a card effect for articles */
.card {
    background-color:  #e0e0d1;
    padding: 20px;
    margin-top: 20px;
}

/*Add a card shadow*/
.card:hover{

   box-shadow: 0 10px 10px 0 rgba(0,0,0,0.2);
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Footer */
.footer {
    padding: 20px;
    text-align: center;
    background: #000;
    margin-top: 20px;
    color: #FFF;
    
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
    .leftcolumn, .rightcolumn {   
        width: 100%;
        padding: 0;
    }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
    .topnav a {
        float: none;
        width: 100%;
    }
}

</style>



</head>
<body data-animate-effect="fadeIn">
<div class="fh5co-loader"></div>
<div class="header">
  <h1>Insight.com</h1>
  <p><i><strong>Join Us and know more about autism</strong></i></p>
 
</div>


<div class="topnav">
  <a href="index.php">Home</a>
  <a href="#">Log In</a>
 
 
</div>

<div class="row">
  <div class="leftcolumn">
    <h2>School and Institution Name </h2>

     <div class="card">

      <a href="http://proyash.edu.bd"> <strong>Proyash</strong> </a>
      <div class="fakeimg">
        <img src="img/1jpg.jpg" alt="Image" style="width:100px; height: 100px" >
      </div>
      <p>
       <strong> Address:</strong> Road - 11, Dhaka Cantonment,Dhaka<br>
        <strong> Email:</strong> info@pr-eb:8888<br>
        <strong> Contact:</strong>  (00) +51278934 <br>
      </p>
    </div>

    <div class="card">
      <a href="https://www.autismwing.com/"><strong> Autism Wing </strong> </a>
      <div class="fakeimg">
        <img src="img/2.png" alt="Image" style="width:100px; height: 95px" >
      </div>
      <p>
        <strong> Address:</strong> House#809, Road#11, Avenue#06, Mirpur DOHS, Dhaka-1216<br>
        <strong> Email:</strong> info@autismwing.com<br>
        <strong> Contact:</strong> +8801716217077, 01976217077 & 01556217077<br>
      </p>
    </div>

     <div class="card">
      <a href="http://www.autism-swacbd.org/"><strong> Society for the welfare of Autistic Children </strong> </a>
      <div class="fakeimg">
        <img src="img/3.jpg" alt="Image" style="width:100px; height: 100px" >
      </div>
      <p>
       <strong> Address:</strong> 70/Ka, Pisciculture, Shaymoli, Dhaka.<br>
       <strong> Email:</strong> info@autism-swacbd.org <br>
        <strong> Contact:</strong>  +88 02 58150565  ,  +88 01711-632861 <br>
      </p>
    </div>

     <div class="card">
      <a href="https://www.facebook.com/SmilingChildrenSpecialSchool/"><strong>  Smiling Children Special School </strong> </a>
      <div class="fakeimg">
        <img src="img/4.jpg" alt="Image" style="width:100px; height: 100px" >
      </div>
      <p>
        <strong> Address:</strong>Kamal House, Jahurul Islam City, House # 40, Road # 06, Block # E,Sector # 01, Aftab Nagar, Badda, Dhaka-1212. <br>
        <strong> Email:</strong>   <br>
        <strong> Contact:</strong> +8804477960897, 01938240167  <br>
      </p>
    </div>

    <div class="card">
      <a href="http://www.acwf-bd.org/"><strong> Autistic Children's Welfare Foundation </strong>  </a>
      <div class="fakeimg">
        <img src="img/5.png" alt="Image" style="width:100px; height: 100px" >
      </div>
      <p>
       <strong> Address:</strong>House no 74, Lane -3, Block - E, Mirpur-12, Dhaka -1216,(Dhaka)<br>
                Mirzapool, Muradpur, Chittagong (Chittagong)<br>
        <strong> Email:</strong> info@acwf-bd.org, acwfbddhaka@gmail.com (Dhaka)   <br>
               acwfbdctg@gmail.com <br>
        <strong> Contact:</strong> +8802-8019238,  +8801914403331/4  (Dhaka)<br>
                +8801819324579 (Chittagong)
      </p>
    </div>

    <div class="card">
      <a href="http://www.crp-bangladesh.org/"><strong>  William and Marie Taylor Inclusive School </strong>  </a>
      <div class="fakeimg">
        <img src="img/6.jpg" alt="Image" style="width:100px; height: 100px" >
      </div>

      <p>
       <strong> Address:</strong> CRP-Chapain, CRP Road, Savar(Savar);Plot # A/5, Block # A, Mirpur # 14(Mirpur)<br>
        <strong> Email:</strong>   <br>
        <strong> Contact:</strong> +880-2-7745464/5 (Savar); 9025563, 01768152922(Mirpur))  <br>
        
      </p>
    </div>



   </div>
      


      
        




  <div class="rightcolumn">
    <div class="card">
      <h2>About Us</h2>
      <div class="fakeimg" style="height:150px;">

          <img src="img/autside.jpg" alt="Image" style="width:225px" >

      </div>
      <p>Insight.com is determined to bring a social change and help people with autism.</p>
    </div>
    <div class="card">
      <h3>Useful Sites</h3>
      <div class="fakeimg"><p><a href="https://www.autismspeaks.org/">
<img border="0" alt="Image" src="img/autismspeak.png" width="220" height="100"></a></p>
      </div>
     
      <div class="fakeimg"><p>

        <a href="https://en.wikipedia.org/wiki/Autism">
<img border="0" alt="Image" src="img/wiki.svg" width="220" height="200"></a>
</p>
    </div>
      <div class="fakeimg"><p> <a href="https://www.autism.org.uk/">
<img border="0" alt="Image" src="img/nas.svg" width="220" height="200"></a></p>
      </div>
          </div>
    
    <div class="card">
      
      <p><strong>Quote of the day!</strong></p>
      <img src="img/quote2.jpg" width="250" height="300">
    </div>
  </div>
</div>

<div class="footer">
  <h4>Contact Us</h4>
  <li>Team Insight.Com</li>
  <li> Dhaka, Bangladesh</li>
</div>

</body>
</html>
