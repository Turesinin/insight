<!DOCTYPE html>
<html>
<head>
<style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial;
    padding: 10px;
    background: #f1f1f1;
}

/* Header/Blog Title */
.header {
    padding: 30px;
    text-align: center;
    background: white;
    background-image: url(img/autheader.jpg);
  

}

.header h1 {
    font-size: 50px;
}

/* Style the top navigation bar */
.topnav {
    overflow: hidden;
    background-color: #333;
}

/* Style the topnav links */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
    float: left;
    width: 75%;
}

/* Right column */
.rightcolumn {
    float: left;
    width: 25%;
    background-color: #f1f1f1;
    padding-left: 20px;
}

/* Fake image */
.fakeimg {
    background-color: #FFF;/* This is the background ogf image color..Before it was ash or #aaa.*/
    width: 100%;
    padding: 20px;
}

/* Add a card effect for articles */
.card {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
}

/*Add a card shadow*/
.card:hover{

   box-shadow: 0 10px 10px 0 rgba(0,0,0,0.2);
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Footer */
.footer {
    padding: 20px;
    text-align: center;
    background: #000;
    margin-top: 20px;
    color: #FFF;
    
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
    .leftcolumn, .rightcolumn {   
        width: 100%;
        padding: 0;
    }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
    .topnav a {
        float: none;
        width: 100%;
    }
}

</style>



</head>
<body>

<div class="header">
  <h1>Insight.com</h1>
  <p><i><strong>Join Us and know more about autism</strong></i></p>
 
</div>


<div class="topnav">
  <a href="index.php">Home</a>
  <a href="#">Log In</a>
 
 
</div>

<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <h2>What is autism?</h2>
      <h5> July 17, 2018</h5>
      <div class="fakeimg" style="height:350px;">

        <img src="img/autism2.jpg" alt="Image" style="width:500px" >

      </div>
    
      <p>Autism is a lifelong developmental disability that affects how people perceive the world and interact with others.

Autistic people see, hear and feel the world differently to other people. If you are autistic, you are autistic for life; autism is not an illness or disease and cannot be 'cured'. Often people feel being autistic is a fundamental aspect of their identity.

Autism is a spectrum condition. All autistic people share certain difficulties, but being autistic will affect them in different ways. Some autistic people also have learning disabilities, mental health issues or other conditions, meaning people need different levels of support. All people on the autism spectrum learn and develop. With the right sort of support, all can be helped to live a more fulfilling life of their own choosing.</p>

<p>
<strong>How common is autism?</strong><br><br>
Autism is much more common than most people think. There are around 700,000 autistic people in the UK - that's more than 1 in 100. People from all nationalities and cultural, religious and social backgrounds can be autistic, although it appears to affect more men than women.
</p>

<p>
  <strong>How do autistic people see the world?</strong>

<p>Some autistic people say the world feels overwhelming and this can cause them considerable anxiety.</p>

<p>In particular, understanding and relating to other people, and taking part in everyday family, school, work and social life, can be harder. Other people appear to know, intuitively, how to communicate and interact with each other, yet can also struggle to build rapport with autistic people. Autistic people may wonder why they are 'different' and feel their social differences mean people don't understand them.</p>

<p>Autistic people often do not 'look' disabled. Some parents of autistic children say that other people simply think their child is naughty, while adults find that they are misunderstood.</p>

</p>

<p><strong>Diagnosis</strong>

  <p>A diagnosis is the formal identification of autism, usually by a multi-disciplinary diagnostic team, often including a speech and language therapist, paediatrician, psychiatrist and/or psychologist.</p>

  <p><strong>The benefits of a diagnosis</strong></p>
  <p>Getting a timely and thorough assessment and diagnosis may be helpful because:</p>
  <p>It helps autistic people (and their families, partners, employers, colleagues, teachers and friends) to understand why they may experience certain difficulties and what they can do about them</p>

  <p>It allows people to access services and support.</p>

</p>
    </div>
    <div class="card">
      <h2>Persistent difficulties with social communication and social interaction</h2>
      <h5>July 17, 2018</h5>
      <div class="fakeimg" style="height:350px;">

           <img src="img/Autismhelp.jpg" alt="Image" style="width:500px" >
      </div>
      <p><strong>SOCIAL COMMUNICATION</strong></p>
      <p>

        Autistic people have difficulties with interpreting both verbal and non-verbal language like gestures or tone of voice. Many have a very literal understanding of language, and think people always mean exactly what they say. They may find it difficult to use or understand:

        <p>facial expressions</p>
        <p>tone of voice</p>
        <p>jokes and sarcasm.</p>

        <p>Some may not speak, or have fairly limited speech. They will often understand more of what other people say to them than they are able to express, yet may struggle with vagueness or abstract concepts. Some autistic people benefit from using, or prefer to use, alternative means of communication, such as sign language or visual symbols. Some are able to communicate very effectively without speech.

        <p>Others have good language skills, but they may still find it hard to understand the expectations of others within conversations, perhaps repeating what the other person has just said (this is called echolalia) or talking at length about their own interests.</p>

        It often helps to speak in a clear, consistent way and to give autistic people time to process what has been said to them.</p>

        <p><strong>Social interaction</strong></p>
        <p>Autistic people often have difficulty 'reading' other people - recognising or understanding others' feelings and intentions - and expressing their own emotions. This can make it very hard for them to navigate the social world. They may:</p>

        <p>appear to be insensitive</p>
        <p>seek out time alone when overloaded by other people</p>
        <p>not seek comfort from other people</p>
        <p>appear to behave 'strangely' or in a way thought to be socially inappropriate.</p>
        <p>Autistic people may find it hard to form friendships. Some may want to interact with other people and make friends, but may be unsure how to go about it.</p>

        <p><big><strong>Restricted and repetitive patterns of behaviours, activities or interests</strong></big></p>
        <p><strong>REPETITIVE BEHAVIOUR AND ROUTINES</strong></p>
        <p>The world can seem a very unpredictable and confusing place to autistic people, who often prefer to have a daily routine so that they know what is going to happen every day. They may want to always travel the same way to and from school or work, or eat exactly the same food for breakfast.

        The use of rules can also be important. It may be difficult for an autistic person to take a different approach to something once they have been taught the 'right' way to do it. People on the autism spectrum may not be comfortable with the idea of change, but may be able to cope better if they can prepare for changes in advance.</p>

        <p><strong>HIGHLY-FOCUSED INTERESTS</strong></p>
        <p>Many autistic people have intense and highly-focused interests, often from a fairly young age. These can change over time or be lifelong, and can be anything from art or music, to trains or computers. An interest may sometimes be unusual. One autistic person loved collecting rubbish, for example. With encouragement, the person developed an interest in recycling and the environment.</p>
        <p>Many channel their interest into studying, paid work, volunteering, or other meaningful occupation. Autistic people often report that the pursuit of such interests is fundamental to their wellbeing and happiness.</p>

        <p><strong>SENSORY SENSITIVITY</strong></p>
        <p>Autistic people may also experience over- or under-sensitivity to sounds, touch, tastes, smells, light, colours, temperatures or pain. For example, they may find certain background sounds, which other people ignore or block out, unbearably loud or distracting. This can cause anxiety or even physical pain. Or they may be fascinated by lights or spinning objects.</p>

        <p><big><strong>Different names for autism</strong></big></p>
        <p>Over the years, different diagnostic labels have been used, such as autism, autism spectrum disorder (ASD), autism spectrum condition (ASC), classic autism, Kanner autism, pervasive developmental disorder (PDD), high-functioning autism (HFA), Asperger syndrome and Pathological Demand Avoidance (PDA). This reflects the different diagnostic manuals and tools used, and the different autism profiles presented by individuals. Because of recent and upcoming changes to the main diagnostic manuals, 'autism spectrum disorder' (ASD) is now likely to become the most commonly given diagnostic term.</p>

        <strong><big>Causes and cures</big></strong></p>
        <p><strong>What causes autism?</strong></p>
        <p>The exact cause of autism is still being investigated. Research into causes suggests that a combination of factors - genetic and environmental - may account for differences in development. Autism is not caused by a person's upbringing, their social circumstances and is not the fault of the individual with the condition.</p>

        <p><strong>Is there a cure?</strong></p>

        <p>There is no 'cure' for autism. However, there is a range of strategies and approaches - methods of enabling learning and development - which people may find to be helpful.</p>

      </p>
    </div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <h2>About Us</h2>
      <div class="fakeimg" style="height:150px;">

          <img src="img/autside.jpg" alt="Image" style="width:225px" >

      </div>
      <p>Insight.com is determined to bring a social change and help people with autism.</p>
    </div>
    <div class="card">
      <h3>Useful Sites</h3>
      <div class="fakeimg"><p><a href="https://www.autismspeaks.org/">
<img border="0" alt="Image" src="img/autismspeak.png" width="220" height="100"></a></p>
      </div>
     
      <div class="fakeimg"><p>

        <a href="https://en.wikipedia.org/wiki/Autism">
<img border="0" alt="Image" src="img/wiki.svg" width="220" height="200"></a>
</p>
    </div>
      <div class="fakeimg"><p> <a href="https://www.autism.org.uk/">
<img border="0" alt="Image" src="img/nas.svg" width="220" height="200"></a></p>
      </div>
          </div>
    
    <div class="card">
      
      <p><strong>Quote of the day!</strong></p>
      <img src="img/quote.jpg" width="250" height="300">
    </div>
  </div>
</div>

<div class="footer">
  <h4>Contact Us</h4>
  <li>Team Insight.Com</li>
  <li> Dhaka, Bangladesh</li>
</div>

</body>
</html>
