<?php
require_once "function.php";
require_once "dbconnect.php";
session_start();
confirmLoggedIn();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>


    
    
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animated.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/imagehover.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/animated.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>




    <!-- Custom Font -->
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font";
            src: url('fonts/BalooThambi-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font1";
            src: url('fonts/Lobster-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        .card2 {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color: #ffffff;
    display: inline-block;
    float: right;
    margin-top: auto;
    max-width: 60%;
    margin-right: 3%;
   
}
.card {
  
  max-width: 30%;
  float: left;
  margin: auto;
    margin-left: 3%;
  text-align: center;
  font-family: arial;
  background-color: rgba(0, 0, 0, 0.0);
}

.card:hover {
    box-shadow: 0 12px 16px 0 rgba(0,0,0,0.2);
}


.title {
  color: wheat;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}



button:hover, a:hover {
  opacity: 0.7;
}
    </style>



    <!--Javascript-->
    <script type = "text/javascript" src= "js/dropdown.js" >
    </script>

    <script type = "text/javascript" src= "js/sticky.js" >
    </script>

    <script src="js/jquery-3.1.1.slim.min.js"></script>
    <script src="js/bootstrap.js"></script>

    <script type = "text/javascript" src= "js/npm.js" >
    </script>

    <script type = "text/javascript" src= "js/tooltip.js" >
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!--js Finishes-->

    <!--Browser Topbar-->
    <link rel="shortcut icon" href="images/img-01.png">

    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>

</head>

<script src = "ckeditor/ckeditor.js"></script>

<!-- php codes here -->
<?php 

       
        $StoringUsername = getUserName();

            $query = "select  UID from user where USER_NAME = '$StoringUsername'";
            $output = mysqli_query($connection, $query);

            while($row = mysqli_fetch_assoc($output)){
     
             $UID = $row['UID']; //Storing user id into UDI variable
            // echo $UID;
                } 

            $get_image_query = "select  image from profile where USER_NAME = '$StoringUsername'"; 

             $output2 = mysqli_query($connection, $get_image_query);

            while($row = mysqli_fetch_assoc($output2)){
     
             $MyImage = $row['image']; //Storing user id into UDI variable
            // echo $UID;
                } 

// COmmented one is the real one....We can use it after the Login is complete...
        if(isset($_POST['editor']))
        {
            
          
            $user = getUserName();
            $text = $_POST['editor'];
           

            $insert = "INSERT INTO blog(USER_NAME,CONTENT,MYID,image) VALUES ('{$user}','{$text}','{$UID}','{$MyImage}')";
            $result = mysqli_query($connection, $insert) or die(mysqli_error());
        
if($result != null){ 
  redirectUserToReadBlog();
};}

?>

<!-----------------------------------------Including the top------------------------->
 <?php include 'navbar.php'; ?>
 <?php 
  
  echo '<h1 style="text-align:center"></h1>';
echo '';
echo '<div class="card">';
 $StoringUsername1 = getUserName();

    $query = "SELECT * FROM profile where USER_NAME= '$StoringUsername1'" ;
                $result = mysqli_query($connection, $query);
                
                    $row = mysqli_fetch_array($result); 

                        echo "<img style='width:100%; border-radius: 500px;'  src='data:image/jpeg;base64,{$row["image"]}'>";
                        $ProfileUserName = $row['USER_NAME'];
                        $MyAddress = $row['ADDRESS'];
                       // echo "<br> <p>{$row["pro_id"]}</p> <br>";
                  
if(!$result){
    $ProfileUserName="Null";
    $MyAddress="Null";
};
//echo '<img src="/w3images/team2.jpg" alt="John" style="width:100%">';
echo '<h2 style= "color: wheat;">'.$ProfileUserName.'</h2>';
echo '<p class="title">'."Profile Card of ". $ProfileUserName.'</p>';
//echo '<p class="title">CEO & Founder, Example</p>';
echo '<p style= "color: wheat;">'. $MyAddress .'</p>';
echo '<div style="margin: 24px 0;">';

echo '</div>';
echo '<p><a href="createprofile.php"><button> Update Profile Picture </button> </a></p>';
echo '</div>';

 ?>

<div class="card2" data-animate-effect="fadeIn">




			

           <form action = "" method="post">
            <textarea class = "ckeditor" name = "editor"></textarea>

         
             <div class="clearfix" >
      <button type="submit" class="signupbtn" value="Submit" name="submit">Submit</button>
    </div>

      

		
		</form>

   
  </div>
</form>


      





</div>
<div id="Footer">

    <img  id="footer_img" class="flip infinite animated" src="logo.png">
    <?php include 'footer.php'; ?> 
</div>

</body>
</html>