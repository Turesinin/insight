<!DOCTYPE html>
<html>
<head>
<style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial;
    padding: 10px;
    background: #f1f1f1;
}

/* Header/Blog Title */
.header {
    padding: 30px;
    text-align: center;
    background: white;
    background-image: url(img/autheader.jpg);
  

}

.header h1 {
    font-size: 50px;
}

/* Style the top navigation bar */
.topnav {
    overflow: hidden;
    background-color: #333;
}

/* Style the topnav links */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

/* Change color on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
    float: left;
    width: 75%;
}

/* Right column */
.rightcolumn {
    float: left;
    width: 25%;
    background-color: #f1f1f1;
    padding-left: 20px;
}

/* Fake image */
.fakeimg {
    background-color: #FFF;/* This is the background ogf image color..Before it was ash or #aaa.*/
    width: 100%;
    padding: 20px;
}

/* Add a card effect for articles */
.card {
    background-color: white;
    padding: 20px;
    margin-top: 20px;
}

/*Add a card shadow*/
.card:hover{

   box-shadow: 0 10px 10px 0 rgba(0,0,0,0.2);
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Footer */
.footer {
    padding: 20px;
    text-align: center;
    background: #000;
    margin-top: 20px;
    color: #FFF;
    
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
    .leftcolumn, .rightcolumn {   
        width: 100%;
        padding: 0;
    }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
    .topnav a {
        float: none;
        width: 100%;
    }
}

</style>



</head>
<body>

<div class="header">
  <h1>Insight.com</h1>
  <p><i><strong>Join Us and know more about autism</strong></i></p>
 
</div>


<div class="topnav">
  <a href="index.php">Home</a>
  <a href="#">Log In</a>
 
 
</div>

<div class="row">
  <div class="leftcolumn">
    <div class="card">
      <h2>Understanding Autism - A short documentary</h2>
      <p>Teebah Foundation- Humanitarian Charity<br>
         Published on Dec 10, 2015</p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/TjVo8ZxiWFU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      
    </div>

    <div class="card">
      <h2>Animated Explanation of Autism</h2>
      <p>Dustin Chandler<br>Published on Apr 20, 2017</p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/6fy7gUIp8Ms" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

    </div>

    <div class="card">
      <h2>What is Autism? Do you know the signs?</h2>
      <p>behaviorfrontiers
        <br>Published on Jun 17, 2011</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/lbXjW-cX9kQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


    </div>

    <div class="card">
      <h2>Early Signs of Autism Video - Oliver 2 1/2 Year Old Boy - Autistic Behavior</h2>
      <p>Kamila Plachetkova<br>
         Published on Oct 10, 2017</p>

         <iframe width="560" height="315" src="https://www.youtube.com/embed/v7JjIKX7bW4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

    <div class="card">
      <h2>"You Don't Look Autistic" Documentary</h2>
      <p>Veganluke<br>
      Published on Jul 29, 2017</p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/Q7q7DUG_B_w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

    <div class="card">
      <h2>Autism — what we know (and what we don't know yet) | Wendy Chung</h2>
      <p>TED <br> Published on Apr 28, 2014</p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/wKlMcLTqRLs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      
    </div>

    




    




  </div>





  <div class="rightcolumn">
    <div class="card">
      <h2>About Us</h2>
      <div class="fakeimg" style="height:150px;">

          <img src="img/autside.jpg" alt="Image" style="width:225px" >

      </div>
      <p>Insight.com is determined to bring a social change and help people with autism.</p>
    </div>
    <div class="card">
      <h3>Useful Sites</h3>
      <div class="fakeimg"><p><a href="https://www.autismspeaks.org/">
<img border="0" alt="Image" src="img/autismspeak.png" width="220" height="100"></a></p>
      </div>
     
      <div class="fakeimg"><p>

        <a href="https://en.wikipedia.org/wiki/Autism">
<img border="0" alt="Image" src="img/wiki.svg" width="220" height="200"></a>
</p>
    </div>
      <div class="fakeimg"><p> <a href="https://www.autism.org.uk/">
<img border="0" alt="Image" src="img/nas.svg" width="220" height="200"></a></p>
      </div>
          </div>
    
    <div class="card">
      
      <p><strong>Quote of the day!</strong></p>
      <img src="img/quote3.jpg" width="250" height="300">
    </div>
  </div>
</div>

<div class="footer">
  <h4>Contact Us</h4>
  <li>Team Insight.Com</li>
  <li> Dhaka, Bangladesh</li>
</div>

</body>
</html>
