<!--
* Created by Abdullah_Kabir on 18/05/2017.
-->
<!DOCTYPE html>
<html>
<head>

    
    
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animated.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/imagehover.css">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/animated.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>




    <!-- Custom Font -->
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font";
            src: url('fonts/BalooThambi-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        @font-face {
            font-family: "My Custom Font1";
            src: url('fonts/Lobster-Regular.ttf') format("truetype");
        }
    </style>
    <style type="text/css">
        .card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color: #ffffff;
    display: inline-block;
}
    </style>



    <!--Javascript-->
    <script type = "text/javascript" src= "js/dropdown.js" >
    </script>

    <script type = "text/javascript" src= "js/sticky.js" >
    </script>

    <script src="js/jquery-3.1.1.slim.min.js"></script>
    <script src="js/bootstrap.js"></script>

    <script type = "text/javascript" src= "js/npm.js" >
    </script>

    <script type = "text/javascript" src= "js/tooltip.js" >
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!--js Finishes-->

    <!--Browser Topbar-->
    <link rel="shortcut icon" href="images/img-01.png">
    <title>Insight</title>

    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>

</head>



<!-----------------------------------------Nav Bar------------------------->
<?php include 'navbar.php'; ?>
<!-------------------------------------- Contents ------------------------------------------------->

<div id="Main_content">
<img src="logo.png" class="img-square" alt="NSU_Retro" width="250" height="200">
    <h2>NSU_Retro</h2>
    <p> We are NSU_Retro, </p>
</div>
<div id="fullContent">
<div class="media" style="width: 100%;margin-top: 05px;">
        <div style="display: inline-block; color: wheat;">
<img src="img/12.jpg" class="img-circle" alt="Abdullah Kabir" width="200" height="200">
    <h2>Abdullah Kabir</h2>
    <p>B.sc Computer Science and Engineering <br>North South University <br> NSU_Retro</p>


</div>

<div style="display: inline-block;color: wheat;padding: 50px;">
<img src="img/10.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
    <h2>Mirza Turesinin</h2>
    <p>B.sc Computer Science and Engineering <br>North South University<br>NSU_Retro</p>
</div>
    

<div style="display: inline-block;color: wheat;">
<img src="img/13.jpg" class="img-circle" alt="Tanzina" width="200" height="200">
    <h2>Tanzina Mollah</h2>
    <p>B.sc Computer Science and Engineering <br>North South University<br>NSU_Retro</p>
</div>
    </div>
</div>

<!---------------------------------------Employee List----------------------------------------------->

<!------------------------------------------Footer---------------------------->
<div id="Footer">

    <img  id="footer_img" class="flip infinite animated" src="logo.png">
    <?php include 'footer.php'; ?> 
</div>

</body>
</html>