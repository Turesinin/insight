<?php
require_once "function.php";
require_once "dbconnect.php";
?>

<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
}

hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

button:hover {
    opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
    padding: 16px;
}

/* Clear floats */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}
.footer {
    padding: 20px;
    text-align: center;
    background: #000;
    margin-top: 20px;
    color: #FFF;
    
}
.header {
    padding: 30px;
    text-align: center;
    background: white;
    background-image: url(img/autheader.jpg);
  

}

.header h1 {
    font-size: 50px;
}
.topnav {
    overflow: hidden;
    background-color: #333;
}

/* Style the topnav links */
.topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
.card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 50%;
    margin-left: 30%;
    margin-top: 2%;
    background-color: #ffffff;
}

/* Change color on hover */
.topnav a:hover {
    background-color: #ddd;
    color: black;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
    .cancelbtn, .signupbtn {
       width: 100%;
    }
}
body{
    background-color: #e79329;
}
</style>
<?php
if (isset($_POST['submit'])) {
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $pass = $_POST['pass'];
    $user = $_POST['username'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $pnumber = $_POST['phone'];

    $insert = "INSERT INTO user(FIRST_NAME, LAST_NAME, PASSWORD, USER_NAME, AGE, EMAIL, ADDRESS, PHONE_NUMBER) VALUES ('{$fname}','{$lname}','{$pass}','{$user}','{$age}','{$email}','{$address}','{$pnumber}')";
    $result = mysqli_query($connection, $insert) or die(mysqli_error());

    if ($result != null) {
        redirectUserToLog();
    }

    else

        echo "<h1>Data was not added </h1>";
}
    ?>
<div class="header">
  <h1>Insight.com</h1>
  <p><i><strong>Join Us and know more about autism</strong></i></p>
 
</div>


<div class="topnav">
  <a href="index.php">Home</a>
  <a href="login.php">Log In</a>
 
 
</div>
<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
    <title>Sign up </title>
    <link rel="stylesheet" href="css/animated.css">
</head>
<body class="fadeIn animated">
<div class="card" data-animate-effect="fadeIn">


<form class="slideInUp animated" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" style="border:1px solid #ccc" >
  <div class="container">
    <h1>Sign Up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="name"><b>First Name</b></label>
    <input type="text" placeholder="First Name" name="first_name" required>

    <label for="name"><b>Last Name</b></label>
    <input type="text" placeholder="Last Name" name="last_name" required>

    <label for="name"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="pass" required>

  <!--  <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="psw-repeat">
-->
    <label for="name"><b>User Name</b></label>
    <input type="text" placeholder="User Name" name="username" required>

    <label for="name"><b>Age</b></label>
    <input type="text" placeholder="age" name="age" required>
    


    <label for="name"><b>Email Address</b></label>
    <input type="text" placeholder="Email Address" name="email" required>

    <label for="name"><b> Address</b></label>
    <input type="text" placeholder="Address" name="address" required>

    <label for="name"><b>Phone Number</b></label>
    <input type="text" placeholder="Phone Number" name="phone" required>

    <select>
        <option>Parent</option>
        <option>Guest</option>
        <option>Doctor</option>
        <option>Institute</option>
    </select>


    
    
    
    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

    <div class="clearfix">
      <button type="submit" class="signupbtn" value="Submit" name="submit">Sign Up</button>
    </div>
  </div>
</form>
</div>
</body>
</html>

<div class="footer">
  <h4>Contact Us</h4>
  <li>Team Insight.Com</li>
  <li> Dhaka, Bangladesh</li>
</div>
