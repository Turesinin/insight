
<footer id="fh5co-footer" class="fh5co-bg" role="contentinfo" style="color: wheat;">
	
		<div class="overlay"></div>
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-4 fh5co-widget">
					<h3>A Little About Insight.</h3>
					<p>Insight is a project by "NSU_Retro", it is a social media where people can discuss openly about autism.</p>
					<p><a class="btn btn-primary" href="blog.php">Post a query</a></p>
				</div>
				<div class="col-md-8">
					<h3>Classes</h3>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<ul class="fh5co-footer-links">
							<li><a href="autism.php">What is Autism</a></li>
							<li><a href="institute.php">Institutions</a></li>
							<li><a href="aboutus.php">About us</a></li>
							<li><a href="contact.php">Contact us</a></li>
						</ul>
					</div>

				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2018, NSU_Retro</small> 
						<small class="block">Designed by NSU_Retro</small>
					</p>
					<p>
						<ul class="fh5co-social-icons" style="text-decoration: none; float: right;">
							<li style="float: right;"><a href="#"><img src="img/tw2.png"></a></li>
							<li style="float: right;"><a href="#"><img src="img/fb2.png"></a></li>
							<li style="float: right;"><a href="#"><img src="img/in2.png"></a></li>
							<li style="float: right;"> <a href="#"><img src="img/insta2.png"></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>

	</footer>
	</div>

	
	